$vCenter = Read-Host -Prompt "Enter the vCenter to connect to: "
$location = Read-Host -Prompt "Enter the path to create CSV: "

Connect-VIServer $vCenter

Get-VM | Select-Object * | Sort-Object Name | Format-Table | Export-Csv -NoTypeInformation -Path $location -ErrorAction Stop
